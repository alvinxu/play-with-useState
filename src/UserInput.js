import './UserInput.scss'

const UserInput = ({onChange}) => {
return<>
    <input onChange={onChange} className='input' type="text"/>
</>
}

export default UserInput