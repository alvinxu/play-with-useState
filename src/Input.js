import {useState} from "react";

export const Input = () => {
    const [isHide, setIsHide] = useState(false)
    const [content, updateContent] = useState('hello')
    return <>
        <h2>please input your password here</h2>
        <input
            type={isHide ? 'password' : 'text'}
            onChange={(e)=>{
                updateContent(e.target.value)
            }}
        />
        <button
            onClick={() => setIsHide(!isHide)}
        >
            {isHide ? 'show' : 'hide'}
        </button>
        <button onClick={()=>updateContent('')}>clear</button>
        {content && <p>{content}</p>}
    </>
}