import './SliderController.scss'
import {useState} from "react";

export const SliderController = () => {
    const [value, setValue] = useState(0.5)
    const [style, updateStyle] = useState({fontSize: '30px', backgroundColor: 'blue'})
    return <>
        <h2>slide to control opacity</h2>
        <h3 style={style}>update complicated state</h3>
        <button onClick={()=>{
            // updateStyle(state => ({...state, backgroundColor: 'yellow'}))
            // is same to
            updateStyle(state => {
                return {
                    ...state,
                    backgroundColor: 'yellow'
                }
            })
        }}
        >
            change style
        </button>
        <div style={{opacity: value}} className='square'/>
        <input
            type="range"
            min={0}
            max={1}
            step={0.1}
            value={value}
            onChange={(e)=>{setValue(e.target.value)}}
        />
    </>
}