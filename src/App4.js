import './App4.scss'
import {Input} from "./Input";
import {SliderController} from "./SliderController";
import {Promotion} from "./Promotion";
import {useState} from "react";

function App4() {
    const [open, setOpen] = useState(true)
    const phone = () => {
        setOpen(false)
    }
    return (
        <div className='appContainer'>
            <h1>This is App page</h1>
            <hr/>
            <Input/>
            <hr/>
            <SliderController/>
            <hr/>
            {open && <Promotion phone={phone}/>}
        </div>
    );
}

export default App4;
