import './Cell.scss'

export const Cell = (props) => {

    const {index, style, onMouseEnter, onClick} = props

    return (
        <div
            className='cell'
            data-index={index}
            style={style}
            onClick={onClick}
            onMouseEnter={onMouseEnter}
        />

    )
}
