import './App2.scss'
import {useState} from "react";

//HW2: create a palette

const App2 = () => {

    const [value, setValue] = useState([255, 255, 255, 1])


    return <>
        <div className="palette"
             style={{
                 backgroundColor:
                     `rgba(${value[0]},${value[1]},${value[2]},${value[3]})`
             }}>
            <div className="controlBox">
                <div className="legend red"/>
                <input
                    type="range"
                    min={0}
                    max={255}
                    step={1}
                    value={value[0]}
                    onChange={(e) =>
                        setValue(Arr => {
                            const newArr = [...Arr]
                            newArr[0] = e.target.value
                            return newArr
                        })}
                />
                <p>{value[0]}</p>
            </div>
            <div className="controlBox">
                <div className="legend green"/>
                <input
                    type="range"
                    min={0}
                    max={255}
                    step={1}
                    value={value[1]}
                    onChange={(e) =>
                        setValue(Arr => {
                            const newArr = [...Arr]
                            newArr[1] = e.target.value
                            return newArr
                        })}
                />
                <p>{value[1]}</p>
            </div>
            <div className="controlBox">
                <div className="legend blue"/>
                <input
                    type="range"
                    min={0}
                    max={255}
                    step={1}
                    value={value[2]}
                    onChange={(e) =>
                        setValue(Arr => {
                            const newArr = [...Arr]
                            newArr[2] = e.target.value
                            return newArr
                        })}
                />
                <p>{value[2]}</p>
            </div>
            <div className="controlBox">
                <div className="legend opacity"/>
                <input
                    type="range"
                    min={0}
                    max={1}
                    step={0.1}
                    value={value[3]}
                    onChange={(e) =>
                        setValue(Arr => {
                            const newArr = [...Arr]
                            newArr[3] = e.target.value
                            return newArr
                        })}
                />
                <p>{value[3]}</p>
            </div>
        </div>
    </>
}

export default App2