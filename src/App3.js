import './App3.scss'
import {Cell} from "./Cell";
import {useState} from "react";

const App3 = () => {

    const cellNumber = 6000

    const [myArr, setMyArr] = useState(
        new Array(cellNumber).fill({}).map(
            (item, index) => ({backgroundColor: `rgba(255,0,0,${1 - index / cellNumber})`})
        ))

    let renderIndex = -1

    // const [toRender, setToRender] = useState(new Array(cellNumber).fill(true))

    const handleMouseEnter = e => {
        let index = parseInt(e.target.getAttribute('data-index'))
        renderIndex = index
        setMyArr((myArr) => {
            const newArr = [...myArr]
            newArr[index] = {backgroundColor: 'black'}
            return newArr
        })
    }

    const handleClick = (e) => {
        window.location.reload()
    }

    return (
        <div className='pageContainer'>
            <div className="container">

                {myArr.map((item, index) =>
                    (renderIndex < 0 || renderIndex === index)
                    && <Cell
                        key={index}
                        index={index}
                        onMouseEnter={handleMouseEnter}
                        onClick={handleClick}
                        style={item}/>
                )
                }

            </div>
        </div>


    )
}
export default App3