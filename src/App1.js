import './App1.scss'
import UserInput from "./UserInput";
import UserOutput from "./UserOutput";
import {useState} from "react";

// HW1:
// 1. Create one Parent component and 2 child components (UserInput, UserOutput)
// 2. In Parent component:there are h1 (title), h3 (instruction) and two child components
// 3. use useState hook to save the value from input from UserInput
// 4. pass the value to UserOutput and convert all letters of the value to uppercase using (value.toUpperCase()) (已编辑)

const App1 = () => {

    const [info, setInfo] = useState('')

    const handleChange = (e) => {
        setInfo(e.target.value.toUpperCase())
    }

    return <>
        <div className='container'>
            <h1>I have 2 children: UserInput and UserOutput</h1>
            <h3>When you type text in UserInput, you'll see that in UserOutput in Uppercase</h3>
            <hr/>
            <label htmlFor="">UserInput:<UserInput onChange={handleChange}/></label>
            <hr/>
            <label htmlFor="output">UserOutput: <UserOutput value={info}/></label>
        </div>
    </>
}

export default App1