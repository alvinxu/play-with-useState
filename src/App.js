import './App.scss'

import {BrowserRouter as Router, Routes, Route, Link} from 'react-router-dom';
import App1 from './App1';
import App2 from './App2';
import App3 from './App3';
import App4 from './App4';

export const App = () => {
    return <>
        <div className='overallContainer'>
            <Router>
                <ul className='list-group'>
                    <li className='list-group-item'>
                        <Link to="in-and-out">User I/O</Link>
                    </li>
                    <li className='list-group-item'>
                        <Link to="palette">Palette</Link>
                    </li>
                    <li className='list-group-item'>
                        <Link to="heatmap">Heat Map</Link>
                    </li>
                    <li className='list-group-item'>
                        <Link to="class16">Something in Class16</Link>
                    </li>
                </ul>
                <Routes>
                    <Route path="in-and-out" element={<App1/>}/>
                    <Route path="palette" element={<App2/>}/>
                    <Route path="heatmap" element={<App3/>}/>
                    <Route path="class16" element={<App4/>}/>
                </Routes>
            </Router>
        </div>
    </>
}

