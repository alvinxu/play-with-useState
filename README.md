React-Data Communication
Parent to Child: props in parent, 
pass argument to child component inside of first <> 
eg: "<WeatherPanel city={'beijing'} isActive={true}/>"
in child, use props to get the values 
eg: {key} inside of () 
eg: "export const WeatherPanel = ({city, isActive=false, phone}) => {}"

you can pass anything 
(including function, html element, component etc...)

child to parent: 
props pass a function (from parent) to child 
where child can call the function to modify the variable in parent

Child to child?

### Homework 1:
 - communication between two children
 - Father pass a function as props to UserInput (Child)
   UserInput use it as onChange() function
   So,when anything change happens on UserInput
   This func will be executed in Father to update a state structure,
   which will make the other child (UserOutput) re-render
 - the use of state, setState, useState

### Homework 2:
- This is an extension of the example in class
- We only have a flat hierarchy here in Father component
  as the children are all elements defined by default,
  e.g. 'div','input','p'
- the onChange function from each 'input' will
  change the state: an array named value stores 4 elements
  representing 'red','green','blue','opacity' respectively
- this homework is another practice of example for pass information
  from children to father

### Homework 3:
- extension from previous practice
- App3 (father) <--> Cell.js (child)
- father <-- child: 
    onMouseEnter event rouses the props function from father, handleMouseEnter()
    where call the hook function setMyArr()
- father --> child:
    pass all the props to child: 
      {style}: object array, representing backgroundColor for each cell
      {index}: a copy of reserved property of 'key'
      {onMouseEnter}: function from father to child
      {onClick}: to reset the page
- As to the hook function: setMyArr()
- Its function is to update the style array: myArr
- Hint: a new array must be copied from the previous one with [...myArr] notion

### Git global setup
git config --global user.name "Alvin Xu"
git config --global user.email "alvinxu@gmail.com"
### Create a new repository
git clone git@gitlab.com:alvinxu/play-with-useState.git
cd play-with-useState
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
### Push an existing folder
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:alvinxu/play-with-useState.git
git add .
git commit -m "Initial commit"
git push -u origin main
### Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:alvinxu/play-with-useState.git
git push -u origin --all
git push -u origin --tags